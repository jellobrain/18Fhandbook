---
title: Writing Lab 101
navtitle: Writing Lab
---

The Writing Lab team knows that writing is hard: It can be time consuming and stress inducing, and can sometimes seem like a blocker to a project that’s humming along. That's why the Writing Lab came into being. The Lab is a virtual writing center where you can get personalized help from members of the 18F editorial team. (And, if you’re a writerly type yourself, you can join the Lab team and volunteer to help other folks with their writing and editing projects!)

## <a id="leadership">Leadership</a>

The 18F Writing Lab is run by members of 18F’s editorial team, which includes members of the Content Guild, the Experience Design Content team (folks who work primarily on partner projects), and the Outreach team (folks who focus on internal and external 18F communication and evangelism).

All Lab members volunteer their services and base their contributions on their availability at a given time. When you file an issue or ask for help in Slack, one of the Writing Lab members will offer to help and assign themselves the issue you create.

The team’s collective experience is vast. Lab members hail from backgrounds in journalism, instructional design, creative writing, public media, and more. Whether you’re thinking about creating site copy or a conference presentation, someone from the Lab has the expertise to scrub in and help.

## <a id="communication">Communication</a>

Find us in Slack:

- [#writing-lab](https://gsa-tts.slack.com/messages/writing-lab)
- [#g-content](https://gsa-tts.slack.com/messages/g-content)

## Frequently asked questions

### <a id="portfolio-of-services">What kind of help can the Writing Lab offer?</a>

TL;DR If you have words and would like help, let us know!

The Lab is happy to offer you generative, developmental, stylistic, or copy editing help on any of the following types of writing:

- Final deliverables (recommendations documents, final reports, presentations decks)
- Mid-process artifacts (interview protocols, screeners, process documentation, research plans, statements of work)
- Playbooks and instructional materials
- Website content
- Blog post
- Talking points
- One sheets
- User interface copy
- Emails
- White papers
- Presentations or slide decks
- Partner agency communications
- #news posts (we can typically turn these around in under 24 hours)
- Content strategy
- And more!

Our role is limited to improving the overall quality of content — its readability, flow, voice, and tone — and doesn't include approving it for publication. After a piece of writing has passed through the Writing Lab, it will be polished and will follow [18F’s Content Guide](https://content-guide.18f.gov/), but that doesn’t necessarily mean it’s ready to be published. Obtaining necessary approvals, designing a place for the content to live, and completing the technical work of posting content are all responsibilities of the content owner.

The Writing Lab is designed for short engagements. If you need more than 10 hours of content help, please [submit a staffing request](https://github.com/18F/staffing-and-resources) to get a content designer assigned to your project.

### <a id="asking-for-help">How do I ask for help?</a>

Standard practice is to file an issue in our [GitHub repo](https://github.com/18F/writing-lab). If you don’t feel comfortable filing an issue, reach out to us on our Slack channel, [#writing-lab](https://gsa-tts.slack.com/archives/writing-lab). Provide us with the basic details of your project and we’ll create an issue for you (and tag you in it). Lab members make edits and specific comments in Google Docs, and they discuss larger questions in comments on the GitHub issue. Whenever possible, Lab members try to pick up and close issues within a week.

This is a lot to digest, so we’ve created this [Writing Lab One Sheet](https://docs.google.com/document/d/1pyP501N6L-mJStTUIhsZ9UQoxy7quzoKND9iibS51ls/edit) for you to keep. It has all the information we think you might need in the future.

### <a id="previous-projects">What has the Writing Lab worked on in the past?</a>

Feel free to peruse the [issues associated with the Writing Lab repository](https://github.com/18F/writing-lab/issues). Also, if you haven’t had a chance to check out the [18F Content Guide](https://pages.18f.gov/content-guide/) yet, that’s a great place to go to familiarize yourself with our style.

### <a id="additional-resources">Additional Resources</a>

Here’s a list of the Lab staff’s favorite resources. Are we missing something? If so, add it to the list.

- [18F Content Guide](https://content-guide.18f.gov/): The official source for 18F’s content policies and preferences
- [18F Blogging Guide](https://handbook.18f.gov/blogging/): Your guide to writing, editing, and publishing posts for the 18F blog
- [The Writing Lab’s performance profile guide](https://docs.google.com/document/d/1z6oyBG43c-5PkK9rAvWeK_bI0ojQqZIJCt8VcmsW53U/edit): An informal guide for editing performance profiles

